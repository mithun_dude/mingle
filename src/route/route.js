import React from 'react';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';



import Intro from '../screens/Initial';
import SignUp from '../screens/SignUp';
import LoginScreen from '../screens/LoginScreen';



const InitialNavigator = createStackNavigator(
    {
        FirstScreen: {
          screen: Intro,
          navigationOptions:{
              header:null,
            }
        },
        SignupPage:  {
          screen : SignUp,
          navigationOptions:{
            headerStyle: { backgroundColor: '#00394D',height:44,borderBottomWidth:1,borderBottomColor:'#102027' },
            headerTintColor: 'white'
          }},

        
      LoginPage   : {
        screen  : LoginScreen,
        navigationOptions:{
          headerStyle: { backgroundColor: '#00394D',height:44,borderBottomWidth:1,borderBottomColor:'#102027' },
          headerTintColor: 'white'
         }},
        
  
   
        });
  
export default createAppContainer(InitialNavigator);
  

  