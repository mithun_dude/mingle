import React, { useState, useEffect } from 'react';

import { emailauth, db } from '../utils/fbconfig'
import { View, Text} from 'react-native';

const ViewEvents = (props) => {

    let [events, setEvents] = useState(null)
    let [eventid,setRegistered] = useState('')
    useEffect(() => {
        db.ref('events').once('value')
            .then(resp => {
                setEvents(resp.val())
            })
            .catch(err => {
                console.log(err)
            })
    }, [])

    useEffect(()=>{
        db.ref('users/'+emailauth.currentUser.uid).once('value')
        .then((resp)=>{
            if(resp.val().event)
                setRegistered(resp.val().event)
        })
        .catch(err => {
            console.log(err)
        })
    },[])

    const registerEvent=(eid)=>{
        db.ref('users/'+emailauth.currentUser.uid).update({event:eid})
        .then(resp=>{setRegistered(eid)})
        .catch((err)=>{console.log(err)})
    }
    return (
        <View style={{ backgroundColor: '#00394D', flex: 1 }}>
            {
                !events ? null :
                    Object.keys(events).map((elem, index) => {
                        return (
                            <Text key={index} style={eventid===elem?{ color: 'green', fontSize: 23 }:{color: 'white', fontSize: 23}}
                            onPress={()=>{registerEvent(elem)}}
                            >
                                {events[elem].name}
                            </Text>
                        )
                    })
            }
        </View>
    )
}

export default ViewEvents