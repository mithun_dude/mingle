import React from 'react'
import {PermissionsAndroid} from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';

import { SafeAreaView } from 'react-native-safe-area-context';
import { View } from 'native-base';


async function requestPermission() {
    try {     
      await PermissionsAndroid.requestMultiple(['android.permission.CAMERA', 'android.permission.WRITE_EXTERNAL_STORAGE'])            
    } catch (err) {
      console.warn(err);
    }
  }
  

const Camera = (props)=>{
    PermissionsAndroid.check('android.permission.CAMERA').then(resp=>{
        if(!resp) requestPermission()
      })      
      return(
          
              <View style={{flex:1}}>
                <QRCodeScanner
                  onRead={props.navigation.state.params.cb}
                />  
              </View>          
      )
}

export default Camera