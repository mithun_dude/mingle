import React, { useState, useEffect } from 'react'
import { emailauth, db } from '../utils/fbconfig'
import { ImageBackground, Image, ToastAndroid, StyleSheet, Platform, StatusBar, View, Text, Dimensions, TouchableOpacity, SafeAreaView } from 'react-native';
import QRCode from 'react-native-qrcode-svg';

const { height, width } = Dimensions.get('window');


const Profile = (props) => {
    let [qrdata, setQr] = useState('')
    let [name, setName] = useState('')

    useEffect(() => {
        db.ref('users/' + emailauth.currentUser.uid).once('value')
            .then(resp => {
                setName(resp.val().name)
            })
    }, [])
    const logOut = () => {
        emailauth.signOut().then(() => {
            props.navigation.navigate('Auth')
        })
    }
    
    const readUser = async (e) => {
        try {
            props.navigation.pop()
            console.log('user', e.data)        
            const friendid = await db.ref('Nodes/' + e.data).once('value')
            console.log('Friend info',friendid.val().id)
            if (friendid.val().id && friendid.val().id != emailauth.currentUser.uid) {
                const resp = await db.ref('users/' + friendid.val().id).once('value')
                await db.ref('users/' + emailauth.currentUser.uid + '/friends/' + friendid.val().id).set({
                    name: resp.val().name,                    
                })
                await db.ref('users/' + friendid.val().id + '/friends/' + emailauth.currentUser.uid).set({
                    name: name
                })
                
                props.navigation.navigate('Friends')
            }
            else {
                ToastAndroid.show('Invalid Id', ToastAndroid.SHORT)
            }

        }
        catch (err) {
            console.log(err)
        }        
    }

    const readQr = async (e) => { //read from the device
        try {
            console.log(e.data)
            const userresp = await db.ref('users/' + e.data).once('value')
            props.navigation.pop()
            console.log(userresp.val())
            props.navigation.navigate('RegisterUser',{userinfo:userresp.val(),uid:e.data,cb:readUser})

        } catch (err) {
            console.log(err)
        }
    }

    return (
        <SafeAreaView style={styles.containersty}>
            <View style={{ justifyContent: 'center', alignItems: 'center', top: 101, backgroundColor: 'orange', width: 160, height: 160 }}>

                <QRCode
                    size={140}
                    ecl="H"
                    value={emailauth.currentUser.uid}
                    logoSize={31}
                    color={'black'}
                    backgroundColor={'white'}
                    getRef={(c) => { setQr(c) }} //base64bitencoding
                />

            </View>
            <View style={{ justifyContent: 'center', alignContent: 'flex-start', top: 105 }}>
                <Text style={styles.capSty}>{name}</Text>
                <Text style={styles.capSty}>{emailauth.currentUser.email}</Text>
                <Text style={styles.capSty}>{emailauth.currentUser.uid}</Text>
                <TouchableOpacity>
                    <View>
                        <Text style={{ color: 'white', fontSize: 23 }} onPress={() => { props.navigation.navigate('CreateEvent') }}>Create Event</Text>
                        <Text style={{ color: 'white', fontSize: 23 }} onPress={() => { props.navigation.navigate('ViewEvents') }}>View Events</Text>
                        <Text style={{ color: 'white', fontSize: 23 }} onPress={() => { props.navigation.navigate('Camera', { cb: readQr }) }}>REGISTER USER</Text>
                    </View>

                </TouchableOpacity>
            </View>
            <View style={{ alignContent: 'flex-end', bottom: 30, position: 'absolute' }}>
                <Text style={{ textAlign: 'center', color: '#fff', fontSize: 23 }} onPress={() => { props.navigation.navigate('Camera', { cb: readUser }) }}>Friends Scanner</Text>
                <Text style={{ textAlign: 'center', color: '#fff', fontSize: 23 }} onPress={() => { props.navigation.navigate('Friends') }}>Friends</Text>
                <Text style={{ textAlign: 'center', color: '#fff', fontSize: 23 }}>Created Events</Text>
                <Text style={{ textAlign: 'center', color: '#fff', fontSize: 23 }} onPress={logOut}>LOGOUT</Text>
            </View>



        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    containersty: { backgroundColor: '#00394D', alignItems: 'center', flex: 1, justifyContent: 'flex-start' },
    picSty: { width: width - 8, height: 250, marginLeft: 12 },
    Hco: { backgroundColor: 'transparent', elevation: 0, shadowOpacity: 0, marginTop: 2, width: 290, height: 80 },
    capSty: {
        color: 'white', fontSize: 16, fontWeight: 'bold', textAlign: 'center', textShadowColor: 'black', textShadowOffset: { width: 3, height: 2 },
        textShadowRadius: 5,
    },
    ButtonViewStyle: { flexDirection: 'row', height: 40, width: width - 37, marginTop: 160, marginBottom: 10 },
    ButtonStyle: { justifyContent: 'center', flexDirection: 'row', alignItems: 'center', backgroundColor: '#45C3B7', flex: 1, flexDirection: 'row', borderRadius: 40, },
    ButtonText: { paddingBottom: 10, textAlign: 'center', fontWeight: 'bold', color: '#00394D', fontSize: 20, marginTop: 10 },
    SignupText: {
        fontSize: 18, textAlign: 'auto', fontWeight: '300', textShadowColor: '#00394D', textShadowOffset: { width: 1, height: 1 },
        textShadowRadius: 5
    },
    loginTextview: { alignContent: 'center', alignItems: 'center', marginVertical: 9, marginBottom: 16 }

});

export default Profile