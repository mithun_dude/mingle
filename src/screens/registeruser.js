import React, { useState, useEffect } from 'react';
import { View, Text } from 'react-native';
import { emailauth, db } from '../utils/fbconfig'

const RegisterUser = (props) => {

    const writeUser = async(e)=>{
        try{
            
            await db.ref('Nodes/'+e.data).set({
                Check:1,
                id:props.navigation.state.params.uid,
                Data1:props.navigation.state.params.userinfo.name,
                Data2:props.navigation.state.params.userinfo.email,
                Data3:'Neoito Technologies'
            })
            props.navigation.state.params.cb(e)
        }catch(err){
            console.log(err)
        }
        
    }
    return (
        <View style={{ backgroundColor: '#00394D', flex: 1 }}>
            <Text style={{ color: 'white', fontSize: 25 }}>
                IN REGISTER: {props.navigation.state.params.userinfo.name}
            </Text>
            {
                props.navigation.state.params.userinfo.event ?
                    <Text
                        style={{ color: 'white', fontSize: 25 }}
                        onPress={() => { props.navigation.navigate('Camera', {cb:writeUser}) }}
                    >
                        Press to Register: {props.navigation.state.params.userinfo.name}
                    </Text>
                    :
                    <Text>User not registered</Text>
            }

        </View>
    )
}

export default RegisterUser