import React, { useState } from 'react';
import { View, Text, Dimensions, StyleSheet, TextInput, TouchableOpacity, ToastAndroid, Button } from 'react-native';

import { emailauth, db } from '../utils/fbconfig'


const { height, width } = Dimensions.get('window');

const SignUp = (props) => {
  let [email, setEmail] = useState('')
  let [password, setPassword] = useState('')

  const loginUser = () => {
    console.log(email)
    if (email && password) {
      emailauth.signInWithEmailAndPassword(email, password)
        .then((resp) => {
          props.navigation.navigate('Profile')
        })
        .catch(errresp => {
          ToastAndroid.show(errresp.code, ToastAndroid.SHORT)
        })
    }
    else {
      ToastAndroid.show('Please fill both text boxes', ToastAndroid.SHORT)
    }
  }

  const createUser = () => {
    console.log(email)
    console.log(password)
    if (email && password) {

      emailauth.createUserWithEmailAndPassword(email, password)
        .then((resp) => {
          db.ref('users/' + resp.user.uid).set({
            name: resp.user.email.split('@')[0],
            email: resp.user.email
          }).then(() => {
            props.navigation.navigate('Profile')
          }).catch(erresp => {
            console.log(erresp)
          })


        })
        .catch(errresp => {
          console.log('sdadasdasdadad->', errresp)
          ToastAndroid.show(errresp.code, ToastAndroid.SHORT)
        })
    }
    else {
      ToastAndroid.show('Please fill both text boxes', ToastAndroid.SHORT)
    }
  }
  return (

    <View style={styles.container}>
      <View style={styles.InitialTextView}>
        <Text style={styles.InitialTextStyle}>To get started, please enter your email and choose a password</Text>
      </View>
      <View style={styles.emailView}>
        <Text style={styles.TextStyle}>Email</Text>

        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start' }}>
          <TextInput style={styles.emailBox}
            placeholder={'h.ibsen@norway.no'}
            onChangeText={setEmail}
          />

        </View>
      </View>
      <View style={{ justifyContent: 'center', paddingVertical: 5, paddingHorizontal: 15 }}>
        <Text style={styles.midText}>(You can change your email later)</Text>
      </View>

      <View style={styles.emailView}>
        <Text style={styles.TextStyle}>Password</Text>

        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start' }}>
          <TextInput style={styles.emailBox}
            placeholder={'*********'}
            onChangeText={setPassword}
          />
        </View>
      </View>

      <View style={styles.finalView}>
       
          <Text style={styles.ButtonText}
            onPress={createUser}
          >
            Create Profile</Text>
       
      </View>

      <View style={styles.finalView}>
       
          <Text style={styles.ButtonText}
            onPress={loginUser}
          >
            Login</Text>
       
      </View>
      <View style={styles.finalTxtView}>
        <Text style={styles.finalTxt}>By continuing you agree that you have read and accepted the <Text style={{ textDecorationLine: 'underline' }} onPress={() => console.warn('it works')} >
          terms and Conditions </Text> and <Text style={{ textDecorationLine: 'underline' }}>Privacy Policy</Text></Text>
      </View>

    </View>
  );

}

export default SignUp;

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#00394D', justifyContent: 'center', color: '#FFFFFF' },
  InitialTextView: { marginVertical: 30, justifyContent: 'center', alignItems: 'center', paddingHorizontal: 15, color: '#FFFFFF' },
  InitialTextStyle: { fontWeight: '400', color: 'white', fontSize: 20, textShadowColor: 'black', textShadowOffset: { width: 2, height: 1 }, textShadowRadius: 5 },
  TextStyle: {
    color: 'white', fontSize: 14, textShadowColor: 'black', textShadowOffset: { width: 2, height: 1 },
    textShadowRadius: 5, justifyContent: 'flex-end', paddingBottom: 4
  },
  emailView: { marginTop: 0, paddingHorizontal: 16, },
  emailBox: { flex: 1, backgroundColor: '#AAAAAA', borderRadius: 10, color: '#FFFFFF', borderColor: 'yellow' },
  lilogo: { borderRadius: 400, paddingLeft: 4, },
  midText: { color: 'rgba(255,255,255,0.6)', fontSize: 14, },
  finalView: { paddingTop: 15, alignContent: "center", paddingHorizontal: 60 },
  buttonView: { backgroundColor: 'rgba(0,206,209,0.6)', borderRadius: 40, alignItems: 'center', height: 38, justifyContent: 'center' },
  ButtonText: {
    fontSize: 18, textAlign: 'auto', fontWeight: '300', textShadowColor: '#00394D', textShadowOffset: { width: 1, height: 1 },
    textShadowRadius: 5
  },
  finalTxtView: { marginTop: 10, alignContent: 'center', paddingHorizontal: 1, width: width - 10 },
  finalTxt: { textAlign: 'center', fontSize: 13, color: 'rgba(0,206,209,0.6)' }
});
