import React, { useState } from 'react';
import { View, Text,StyleSheet,TouchableOpacity,Image,Dimensions,SafeAreaView } from 'react-native';
import imageIndex from '../images/imageIndex';

import {PassBox,InputBox} from '../components/UsernameBox'

const {height, width} = Dimensions.get('window');





const LoginScreen = (props) =>{
    return(
        <SafeAreaView style={styles.containersty}>
            <View style = {styles.InitialTextView}>
                <Text style = {styles.InitialTextStyle}>Login</Text>
            </View>

            <View style={styles.emailView}>
             <Text style={styles.emailStyle}>Email</Text>
             <InputBox Holder={'Username'}/>
             <View style={styles.emailView}>
          <Text style={styles.passStyle}>Password</Text>
          </View>
             <PassBox Holder={'Password'}/>
            
            
            </View>
       

            
                <TouchableOpacity style={styles.buttonView}>
                     <Text style={styles.ButtonText}>Login</Text>
                </TouchableOpacity>
            
            <TouchableOpacity>
            <View style = {styles.passFpart}>
                <Text style={styles.TextStyle}>Forgot your password?</Text>
            </View>
            </TouchableOpacity>
            
            <View style = {styles.passFpart}>
                <Text>Or, long in with: </Text>
            </View>

            <View style ={styles.logoPlacer}>
            <TouchableOpacity style={styles.lilogo}>
            <Image
              source = {imageIndex.splashScreen.fblogo}
              style = {{width:40,height:40}}
            />
          </TouchableOpacity>
          <TouchableOpacity style={styles.logoPlacer}>
          <Image
              source = {imageIndex.splashScreen.gmaillogo}
              style ={{width:40,height:40}}
            />
          </TouchableOpacity>
            </View>


        </SafeAreaView>

    );
}




export default LoginScreen;

const styles = StyleSheet.create({
    containersty:{backgroundColor:'#00394D',flex:1,justifyContent:'flex-start',},
    InitialTextView:{marginLeft:5,marginTop:'30%'},
    InitialTextStyle:{textAlign:'justify',fontWeight:'400',color:'white',fontSize:20,textShadowColor:'black',textShadowOffset: { width: 2, height: 1 },textShadowRadius:5,marginLeft:6,marginVertical:10},
    TextStyle:{color:'white',fontSize:14,textShadowColor:'black',textShadowOffset: { width: 2, height: 1 },
    textShadowRadius: 5,paddingBottom:4},
    emailStyle:{color:'white',fontSize:12,textShadowColor:'black',textShadowOffset: { width: 2, height: 1 },
    textShadowRadius: 5,paddingHorizontal:31,marginTop:5,marginBottom:5},
    passStyle:{color:'white',fontSize:12,textShadowColor:'black',textShadowOffset: { width: 2, height: 1 },
    textShadowRadius: 5,paddingHorizontal:16,marginTop:5,marginBottom:5},
    emailView:{marginTop:0,paddingHorizontal:16,},
    finalView:{paddingTop:15,alignContent:"center",paddingHorizontal:60},
    buttonView:{marginHorizontal:width/4,backgroundColor:'rgba(0,206,209,0.6)',borderRadius:40,alignItems:'center',height:38,justifyContent:'center',width:width-160,marginTop:5},
    ButtonText:{fontSize:18,textAlign:'auto',fontWeight:'300',textShadowColor:'#00394D',textShadowOffset: { width: 1, height: 1 },
    textShadowRadius: 5},
    logoPlacer:{justifyContent:'center',flexDirection:'row',marginBottom:10,marginLeft:5},
    lilogo:{borderRadius:400,paddingLeft:4,flexDirection:'row'},
    passFpart:{marginBottom:17,alignItems:'center',marginVertical:3,textShadowColor:'black',textShadowOffset: { width: 2, height: 1 },}
    
});