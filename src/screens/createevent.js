import React,{useState} from 'react';

import {emailauth,db} from '../utils/fbconfig'
import { View, Text , ToastAndroid} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';

const CreateEvent = (props)=>{
let [ename,setEname] = useState('')
let [evenue,setEvenue] = useState('')

const createEvent = ()=>{
    db.ref('events').push({
        name:ename,
        venue:evenue,
        mail:emailauth.currentUser.email
    }).then(()=>{
        ToastAndroid.show('Succcess',ToastAndroid.SHORT)
    })
    .catch(err=>{
        console.log(err)
    })
}
    return(
        <View style={{backgroundColor:'#00394D',flex:1}}>
            <TextInput
                style={{borderColor:'white',color:'white',borderStyle:'solid'}}
                placeholder={'Event name'}
                onChangeText={setEname}
            />
            <TextInput
                style={{borderColor:'white',color:'white'}}
                placeholder={'Event venue'}
                onChangeText={setEvenue}
            />
            <Text style={{color:'white',fontSize:20}} onPress={createEvent}>Create Event</Text>
        </View>
    )
}

export default CreateEvent