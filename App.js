import {createSwitchNavigator } from 'react-navigation';
import { createAppContainer } from 'react-navigation';

import {appstack,authstack} from './src/route/routing'

import Splash from './src/screens/Splash'

export default createAppContainer(
  createSwitchNavigator({
    splash:Splash,
    Auth:authstack,
    App:appstack
  },
  {
    initialRouteName:'splash'
  }
  )
)