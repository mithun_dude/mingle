import React, { useState, useEffect } from 'react';
import { View, Text} from 'react-native';
import { emailauth,db } from '../utils/fbconfig'


const Friends = (props) => {
    
    let [friends,setFriends] = useState(null)

    useEffect(()=>{
        db.ref('users/'+emailauth.currentUser.uid+'/friends').once('value')
        .then(resp=>{
            if(resp.val()){
                setFriends(resp.val())
                console.log(resp.val())
            }
            
        })
        .catch(err=>{console.log(err)})
    },[])

    return(
        <View style={{ backgroundColor: '#00394D', flex: 1 }}>
            <Text style={{color:'white',fontSize:23}}>
                FRIENDS
            </Text>
            <View style={{paddingLeft:7}}>
            {!friends?null:
                Object.keys(friends).map((elem)=>{
                    return(
                        <Text style={ {color: 'white', fontSize: 29} }>
                            {friends[elem].name}
                        </Text>
                    )
                })
            }
            </View>
        </View>
    )
}

export default Friends