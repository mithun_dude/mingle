import app from 'react-native-firebase'

// import  'react-native-firebase/database'


var firebaseConfig = {
  apiKey: "AIzaSyBckcLfHOz7dQDeQ0-I4be3HsKZabEDHXw",
  authDomain: "android-projekt-1.firebaseapp.com",
  databaseURL: "https://android-projekt-1.firebaseio.com",
  projectId: "android-projekt-1",
  storageBucket: "android-projekt-1.appspot.com",
  messagingSenderId: "939317676821",
  appId: "1:939317676821:web:ebfd34ee8248f9c777f71c",
  measurementId: "G-5GPJ6XJ0QY"
};
  
app.initializeApp(firebaseConfig)

  // export default firebase
  export const db = app.database();
  export const emailauth = app.auth()