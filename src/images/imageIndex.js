const imageIndex = {
    splashScreen: {
        first : require('./mingl.png'),
        second: require('./mingl_blue.png'),
        headpic: require('./headerpart.png'),
        midp: require('./midpart.png'),
        lilogo: require('../images/linkedin_icon.png'),
        fblogo: require('../images/fb-logo.png'),
        gmaillogo: require('../images/gmail-logo.png')
    }
    }


export default imageIndex;