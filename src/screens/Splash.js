import React,{useEffect} from 'react';
import { SafeAreaView,StyleSheet, Image,View } from 'react-native';
import imageIndex from '../images/imageIndex';

import {emailauth} from '../utils/fbconfig'


const Splash = (props) =>{

  
  useEffect(()=>{
    setTimeout(()=>{    
        props.navigation.navigate(emailauth.currentUser?'App':'Auth')
    },997)
  },[])

    return(
            <SafeAreaView style={styles.container}>
              {
                <View style={styles.imgContainer}>
                <Image source={imageIndex.splashScreen.first} style={styles.im} ></Image>
                </View>
              }
              
            </SafeAreaView>

    );
}


const styles = StyleSheet.create({
    container:{flex:1,backgroundColor:'#00394D',justifyContent:'center',alignItems:'center',},
    imgContainer:{height:90,width:120},
    im:{width:null,height:null,overflow:'hidden',flex:1}
});

export default Splash


//backgroundColor:'#263a4d',