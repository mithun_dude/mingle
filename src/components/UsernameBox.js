import React,{useState} from 'react';
import {StyleSheet,View,TextInput,Dimensions,TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const {height, width} = Dimensions.get('window');





// const [eyeIcon,seteyeIcon]= useState([
//     {initial:ios-eye},
    
// ]);




export const InputBox = (props) => {
   let  [email,setEmail] = useState("");  
    return(

<View style={styles.inputContainer}>
            <Icon name = {'ios-person'} size={28} color='rgba(225,225,225,0.7)' style={styles.inputIcon}/>    
                <TextInput
                    style = {styles.input}
                    placeholder={props.Holder}
                    placeholderTextColor='rgba(225,225,225,0.7)'
                    underlineColorAndroid='transparent'
                    onChangeText={setEmail}
                    value = {email}
                    />
                    
            </View>
);} 
export const PassBox = (props) => {
    let [Visibility,setVisibility] = useState(true)
    let [password,setPassword]= useState("")

    const visibilityIconSet = ()=>{
        setVisibility(!Visibility)
    }
return(
            <View style={styles.inputContainer}>
            <Icon name = {'ios-lock'}  size={28} color='rgba(225,225,225,0.7)' style={styles.inputIcon}/>    
                <TextInput
                    style = {styles.input}
                    placeholder={props.Holder}
                    secureTextEntry={Visibility}
                    placeholderTextColor='rgba(225,225,225,0.7)'
                    underlineColorAndroid='transparent'
                    onChangeText={setPassword}
                    value = {password}
                />
                <TouchableOpacity style={styles.eyebtn}
                 onPress ={()=>setVisibility}>
                    <Icon name={Visibility === true ? 'ios-eye' :'ios-eye-off'} onPress={visibilityIconSet} size={26} color={'rgba(255,255,255,0.7)'} />
                </TouchableOpacity>
            </View>
);}            

const styles = StyleSheet.create(
    {
    input:{width:width - 55,height: 40, borderRadius: 45,fontSize:16,paddingLeft: 45,backgroundColor:'rgba(0,0,0,0.35)',color:'#ffffff' ,marginLeft:'4%'},
    inputIcon:{position:'absolute',top:5,left:28},
    inputContainer:{marginTop:0,marginBottom:3},
    eyebtn:{position:'absolute',top:5,right:25},
    });
