import React from 'react';
import { ImageBackground,Image,StyleSheet,Platform,StatusBar,View,Text,Dimensions,TouchableOpacity,SafeAreaView } from 'react-native';
import {Container,Header,Content,Button} from 'native-base';
import imageIndex from '../images/imageIndex';


const {height, width} = Dimensions.get('window');


const Intro = (props) => {
   return(
            <SafeAreaView style={styles.containersty}>
                
                <ImageBackground source = {imageIndex.splashScreen.first} style={{width:null,height:null,overflow:"hidden",marginTop:20,flexDirection:'row'}}>
                    <View style={{width:160,height:90,}}/>
                </ImageBackground>

        
                <View style={{justifyContent:'center',alignContent:'flex-start'}}>
                    <Image source= {imageIndex.splashScreen.midp} style={[styles.picSty]}/>
                </View>
                
               <View style={{alignItems:'center',justifyContent:'flex-end',flexDirection:'row'}}>
                   <Text style={styles.capSty}>Waste no time,superfast contact sharing</Text>
               </View>

               <View style={{position:'absolute',bottom:10}}>
                 <View style={styles.ButtonViewStyle}>
                 <TouchableOpacity style={styles.ButtonStyle}
                 onPress={()=>props.navigation.navigate({routeName:'SignupPage'})} >

                       <Text style={[styles.ButtonText,styles.SignupText]}>Sign up</Text>
                 </TouchableOpacity>   
               </View> 


                <View style={styles.loginTextview}>
                    <TouchableOpacity   
                    onPress={()=>props.navigation.navigate({routeName:'LoginPage'})} 
                    >
                    <Text style={styles.capSty} >Already have an account? Login</Text>
                    </TouchableOpacity>
                </View>

                </View>    
            
                </SafeAreaView>                
            

          

   );
}

export default Intro;

const styles = StyleSheet.create({
    containersty:{backgroundColor:'#00394D',alignItems:'center',flex:1,justifyContent:'flex-start'},
    picSty:{width:width-8,height:250,marginLeft:12},
    Hco:{backgroundColor:'transparent',elevation:0,shadowOpacity:0,marginTop:2,width:290,height:80},
    capSty:{color:'white',fontSize:16,fontWeight:'bold',textAlign:'center',textShadowColor:'black',textShadowOffset: { width: 3, height: 2 },
    textShadowRadius: 5,justifyContent:'flex-end',},
    ButtonViewStyle:{flexDirection:'row',height:40,width:width-37,marginTop:160,marginBottom:10},
    ButtonStyle:{justifyContent:'center',flexDirection:'row',alignItems:'center',backgroundColor:'#45C3B7',flex:1,flexDirection:'row',borderRadius:40,},
    ButtonText:{paddingBottom:10,textAlign:'center',fontWeight:'bold',color:'#00394D',fontSize:20,marginTop:10},
    SignupText:{fontSize:18,textAlign:'auto',fontWeight:'300',textShadowColor:'#00394D',textShadowOffset: { width: 1, height: 1 },
    textShadowRadius: 5},
    loginTextview:{alignContent:'center',alignItems:'center',marginVertical:9,marginBottom:16}

});


// backgroundColor:'#00394D'