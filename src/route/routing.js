
import { createStackNavigator } from 'react-navigation-stack';

import SignUp from '../screens/SignUp';
import LoginScreen from '../screens/LoginScreen';
import Profile from '../screens/profile'; //change later
import Camera from '../screens/camera';
import CreateEvent from '../screens/createevent';
import ViewEvents from '../screens/viewevents';
import Friends from '../screens/friends';
import RegisterUser from '../screens/registeruser';

export const authstack = createStackNavigator(
    {
        signup: {
            screen: SignUp,
            navigationOptions: {
                header: null,
            }
        },
        login: {
            screen: LoginScreen,
            navigationOptions: {
                header: null,
            }
        }
    }
)

export const appstack = createStackNavigator(
    {
        Profile: {
            screen: Profile,
            navigationOptions: {
                header: null,
            }            
        },
        Camera: {
            screen: Camera,
            navigationOptions: {
                header: null,
            }
        },
        CreateEvent:{
            screen:CreateEvent,
            navigationOptions: {
                header: null,
            }
        },
        ViewEvents:{
            screen:ViewEvents,
            navigationOptions: {
                header: null,
            }
        },
        Friends:{
            screen:Friends,
            navigationOptions: {
                header: null,
            }
        },
        RegisterUser:{
            screen:RegisterUser,
            navigationOptions: {
                header: null,
            }
        }
    }
)
